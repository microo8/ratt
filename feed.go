package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gorilla/feeds"
	lua "github.com/yuin/gopher-lua"
)

func getDocFromUrl(link *url.URL) (*goquery.Document, error) {
	client := GlobalHTTPSettings.Client()
	req, err := http.NewRequest("GET", link.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("creating request: %w", err)
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("requesting site data: %w", err)
	}
	if CLI.Verbose {
		d, _ := httputil.DumpRequest(req, true)
		log.Println(string(d))
		d, _ = httputil.DumpResponse(resp, true)
		log.Println(string(d))
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s", resp.StatusCode, resp.Status)
	}
	//io.Copy(os.Stdout, resp.Body)
	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		return nil, fmt.Errorf("parsing site data: %w", err)
	}
	return doc, nil
}

func constructFeed(url *url.URL, selectors Selectors) (feed *feeds.Feed, err error) {
	doc, err := getDocFromUrl(url)
	if err != nil {
		return nil, err
	}
	feed = &feeds.Feed{
		Link:    &feeds.Link{Href: url.String()},
		Created: time.Now(),
	}
	feed.Title, err = getSelData(doc.Selection, selectors.Feed.Title, url.String())
	if err != nil {
		return nil, fmt.Errorf("feed title: %w", err)
	}
	if selectors.Feed.Description != "" {
		feed.Description, err = getSelData(doc.Selection, selectors.Feed.Description, url.String())
		if err != nil {
			return nil, fmt.Errorf("feed description: %w", err)
		}
	}
	if selectors.Feed.AuthorName != "" {
		authorName, err := getSelData(doc.Selection, selectors.Feed.AuthorName, url.String())
		if err != nil {
			return nil, fmt.Errorf("feed author name: %w", err)
		}
		if feed.Author == nil {
			feed.Author = &feeds.Author{}
		}
		feed.Author.Name = authorName
	}
	if selectors.Feed.AuthorEmail != "" {
		authorEmail, err := getSelData(doc.Selection, selectors.Feed.AuthorEmail, url.String())
		if err != nil {
			return nil, fmt.Errorf("feed author email: %w", err)
		}
		if feed.Author == nil {
			feed.Author = &feeds.Author{}
		}
		feed.Author.Email = authorEmail
	}

	var wa sync.WaitGroup
	//if item containter selector is a multiline value, then it is lua code that rerutns a number
	if strings.ContainsRune(selectors.Item.Container, '\n') {
		output, err := runLua(nil, selectors.Item.Container, url.String())
		if err != nil {
			return nil, fmt.Errorf("item container lua code: %w", err)
		}
		num, err := strconv.Atoi(output)
		if err != nil {
			return nil, fmt.Errorf("item container lua output is not number: %w", err)
		}
		wa.Add(num)
		feed.Items = make([]*feeds.Item, num)
		for i := 0; i < num; i++ {
			i := i
			go func() {
				defer wa.Done()
				it, itemErr := getItem(nil, selectors, url.String(), addGlobal("index", lua.LNumber(i)))
				if itemErr != nil {
					err = itemErr
					return
				}
				if it == nil {
					return
				}
				feed.Items[i] = it
			}()
		}
	} else {
		//else it is a css selector and selects all the item containers
		doc.Find(selectors.Item.Container).EachWithBreak(
			func(index int, item *goquery.Selection) bool {
				wa.Add(1)
				go func() {
					defer wa.Done()
					i, itemErr := getItem(item, selectors, url.String(), addGlobal("index", lua.LNumber(index)))
					if itemErr != nil {
						err = itemErr
						return
					}
					if i == nil {
						return
					}
					feed.Items[index] = i
				}()
				feed.Items = append(feed.Items, nil)
				return true
			},
		)
	}
	wa.Wait()
	if err != nil {
		return nil, fmt.Errorf("constructing item: %w", err)
	}

	if selectors.NextPage == "" || selectors.NextPageCount <= 0 {
		return feed, nil
	}

	var nextLink string
	if selectors.NextPageAttr != "" {
		nextLink, err = getSelAttr(doc.Selection, selectors.NextPage, selectors.NextPageAttr, url.String())
		if err != nil {
			return nil, fmt.Errorf("getting next page link from attr: %w", err)
		}
	} else {
		nextLink, err = getSelData(doc.Selection, selectors.NextPage, url.String())
		if err != nil {
			return nil, fmt.Errorf("getting next page link: %w", err)
		}
	}
	nextURL, err := url.Parse(nextLink)
	if err != nil {
		return nil, fmt.Errorf("parsing next page link: %w", err)
	}
	selectors.NextPageCount -= 1
	nextFeed, err := constructFeed(nextURL, selectors)
	if err != nil {
		return nil, fmt.Errorf("constructing feed from (%s): %w", nextLink, err)
	}
	feed.Items = append(feed.Items, nextFeed.Items...)
	return feed, nil
}

func getItem(sel *goquery.Selection, selectors Selectors, url string, luaOptions ...luaOption) (item *feeds.Item, err error) {
	item = &feeds.Item{}
	item.Title, err = getSelData(sel, selectors.Item.Title, url, luaOptions...)
	if err != nil {
		dumpSelectionData(sel)
		return nil, fmt.Errorf("item title: %w", err)
	}
	if selectors.Item.Description != "" {
		item.Description, err = getSelData(sel, selectors.Item.Description, url, luaOptions...)
		if err != nil {
			dumpSelectionData(sel)
			return nil, fmt.Errorf("item description: %w", err)
		}
	}
	itemLink, err := getSelAttr(sel, selectors.Item.Link, selectors.Item.LinkAttr, url, luaOptions...)
	if err != nil {
		dumpSelectionData(sel)
		fmt.Fprintf(os.Stderr, "\nERROR: item link: %s\n", err)
		return nil, nil
	}
	item.Link = &feeds.Link{Href: itemLink}
	if selectors.Item.CreatedFormat != "" {
		itemCreated, err := getSelData(
			sel,
			selectors.Item.Created,
			url,
			luaOptions...,
		)
		if err != nil {
			dumpSelectionData(sel)
			return nil, fmt.Errorf("item created time: %w", err)
		}
		if itemCreated != "" {
			item.Created, err = time.Parse(selectors.Item.CreatedFormat, itemCreated)
			if err != nil {
				dumpSelectionData(sel)
				return nil, fmt.Errorf("item parsing created time: %w", err)
			}
		}
	}
	if selectors.Item.Image != "" {
		imageURL, err := getSelAttr(
			sel,
			selectors.Item.Image,
			selectors.Item.ImageAttr,
			url,
			luaOptions...,
		)
		if err != nil {
			dumpSelectionData(sel)
			fmt.Fprintf(os.Stderr, "item image: %s\n", err)
		}
		item.Enclosure = &feeds.Enclosure{Url: imageURL, Length: "0", Type: "image/jpeg"}
	}
	return item, nil
}

func getSelData(sel *goquery.Selection, selector, url string, luaOptions ...luaOption) (string, error) {
	if strings.ContainsRune(selector, '\n') {
		return runLua(sel, selector, url, luaOptions...)
	}
	s := sel.Find(selector)
	if s.Length() == 0 {
		dumpSelectionData(sel)
		return "", fmt.Errorf("no html node found for selector: '%s'", selector)
	}
	return s.First().Text(), nil
}

//returns nodes data, or if data is empty, returns nodes attr data
func getSelAttr(sel *goquery.Selection, selector, attr, url string, luaOptions ...luaOption) (string, error) {
	if strings.ContainsRune(selector, '\n') {
		return runLua(sel, selector, url, luaOptions...)
	}
	s := sel.Find(selector)
	if s.Length() == 0 {
		return "", fmt.Errorf("no html node found for selector: '%s'", selector)
	}
	val, exists := s.First().Attr(attr)
	if !exists {
		return "", fmt.Errorf("no data found for selector: '%s'", selector)
	}
	return val, nil
}

func dumpSelectionData(sel *goquery.Selection) {
	if sel == nil {
		return
	}
	data, _ := goquery.OuterHtml(sel)
	fmt.Fprintf(os.Stderr, "\nSelection data: %s\n", data)
}
