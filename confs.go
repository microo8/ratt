package main

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gopkg.in/yaml.v2"
)

type conf struct {
	Regex string
	Selectors
}

func save(filepath, regex string, selectors Selectors) {
	conf := conf{
		Regex:     regex,
		Selectors: selectors,
	}
	f, err := os.Create(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	if err := yaml.NewEncoder(f).Encode(conf); err != nil {
		log.Fatal(err)
	}
}

func findSelectors(url string) (Selectors, error) {
	confs, err := fs.Sub(confs, "confs")
	if err != nil {
		return Selectors{}, err
	}
	selectors, err := findSelectorsInDir(url, "embededConfs", confs)
	if err == nil {
		return selectors, nil
	}
	selectors, err = findSelectorsInDir(url, "currentDir/", os.DirFS("."))
	if err == nil {
		return selectors, nil
	}
	home, err := os.UserHomeDir()
	if err != nil {
		return Selectors{}, err
	}
	homeConf := filepath.Join(home, ".config", "ratt")
	selectors, err = findSelectorsInDir(url, homeConf, os.DirFS(homeConf))
	if err != nil {
		if os.IsNotExist(err) {
			return Selectors{}, fmt.Errorf("no conf found for %s", url)
		}
		return Selectors{}, err
	}
	return selectors, nil
}

func findSelectorsInDir(url, dirname string, dir fs.FS) (Selectors, error) {
	dirs, err := fs.ReadDir(dir, ".")
	if err != nil {
		return Selectors{}, err
	}
	for _, d := range dirs {
		if d.IsDir() {
			continue
		}
		if !strings.HasSuffix(d.Name(), ".yml") {
			continue
		}
		f, err := dir.Open(d.Name())
		if err != nil {
			return Selectors{}, err
		}
		var conf conf
		if err := yaml.NewDecoder(f).Decode(&conf); err != nil {
			return Selectors{}, err
		}
		re, err := regexp.Compile(conf.Regex)
		if err != nil {
			return Selectors{}, err
		}
		if CLI.Verbose {
			log.Printf("checking regex: %s/%s", dirname, d.Name())
		}
		if !re.MatchString(url) {
			continue
		}
		if CLI.Verbose {
			log.Printf("using config: %s/%s", dirname, d.Name())
		}
		return conf.Selectors, nil
	}
	return Selectors{}, fmt.Errorf("didn't found conf for url: '%s'", url)
}
