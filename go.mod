module gitlab.com/microo8/ratt

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/alecthomas/kong v0.2.16
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/gorilla/feeds v1.1.1
	github.com/itchyny/gojq v0.12.3
	github.com/pkg/errors v0.9.1 // indirect
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da
	golang.org/x/net v0.0.0-20210428140749-89ef3d95e781 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0
)
