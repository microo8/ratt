package main

import (
	"embed"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/alecthomas/kong"
)

//go:embed confs/*
var confs embed.FS

var CLI struct {
	Extract struct {
		Selectors  Selectors `embed`
		OutputType string    `short:"t" enum:"rss,atom,json" default:"rss" help:"output type (rss/atom/json)"`
		URL        *url.URL  `xor:"1" arg name:"url" help:"Site url."`
	} `cmd help:"extract feed from side using specified selectors"`
	Save struct {
		Selectors Selectors `embed`
		UrlRegex  string    `required arg help:"regex of the website url for the config file"`
		Path      string    `arg type:"path" help:"config file path"`
	} `cmd help:"save selectors to a config file"`
	Auto struct {
		OutputType string   `short:"t" enum:"rss,atom,json" default:"rss" help:"output type (rss/atom/json)"`
		URL        *url.URL `xor:"1" arg name:"url" help:"Site url."`
	} `cmd help:"automatically will try to find config for the website url (searches internal config files, in current directory and in ~/.config/ratt/*.conf)"`
	DataURLEncode string `optional name:"data-urlencode" help:"adds new url-encoded values to url"`
	Verbose       bool   `optional default:"false" short:"v" help:"will print out the full http requests and responses"`
}

type Selectors struct {
	HTTPSettings HTTPSettings `yaml: "httpsettings" embed`
	Feed         struct {
		Title       string `required name:"feed-title" help:"css selector for the feed title"`
		Description string `optional name:"feed-description" help:"css selector for the feed description"`
		AuthorName  string `optional help:"css selector for the feed author name"`
		AuthorEmail string `optional help:"css selector for the feed author email"`
	} `yaml:"feed" embed`
	Item struct {
		Container     string `required name:"item-container" help:"css selector for the item container"`
		Title         string `required name:"item-title" help:"css selector for the item title"`
		Link          string `required name:"item-link" help:"css selector for the item link"`
		LinkAttr      string `default:"href" name:"item-link-attr" help:"get attribute value of the item link element"`
		Created       string `required name:"item-created" help:"css selector for the item created time"`
		CreatedFormat string `required name:"item-created-format" help:"css selector for the item created time format"`
		Description   string `name:"item-description" help:"css selector for the item description"`
		Image         string `name:"item-image" help:"css selector for the item image"`
		ImageAttr     string `name:"item-image-attr" default:"src" help:"get attribute value of the item image element"`
	} `yaml:"item" embed`
	NextPage      string `optional help:"css selector for the link to the next page to be scraped"`
	NextPageAttr  string `optional default:"href" help:"get attribute value of the next page element"`
	NextPageCount int    `optional help:"how deep to follow the next page link (integer value)"`
}

var GlobalHTTPSettings *HTTPSettings

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	ctx := kong.Parse(&CLI,
		kong.Name("ratt"),
		kong.Description("RSS all the things!\nA small html to rss/atom/json feed generator"),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: true,
			Summary: true,
		}))

	switch ctx.Command() {
	case "extract <url>":
		GlobalHTTPSettings = &CLI.Extract.Selectors.HTTPSettings
		extract(CLI.Extract.URL, CLI.Extract.Selectors, CLI.Extract.OutputType)
	case "save <url-regex> <path>":
		save(CLI.Save.Path, CLI.Save.UrlRegex, CLI.Save.Selectors)
	case "auto <url>":
		selectors, err := findSelectors(CLI.Auto.URL.String())
		if err != nil {
			log.Fatal(err)
		}
		GlobalHTTPSettings = &selectors.HTTPSettings
		extract(CLI.Auto.URL, selectors, CLI.Auto.OutputType)
	}
}

func addQuery(u *url.URL, data string) {
	if data == "" {
		return
	}
	if strings.Contains(data, "=") {
		for _, kv := range strings.Split(data, "&") {
			d := strings.Split(kv, "=")
			k, v := d[0], d[1]
			q := u.Query()
			q.Set(k, v)
			u.RawQuery = q.Encode()
		}
		return
	}
	u.Path += strings.ReplaceAll(data, " ", "+")
}

func extract(link *url.URL, selectors Selectors, outputType string) {
	addQuery(link, CLI.DataURLEncode)
	feed, err := constructFeed(link, selectors)
	if err != nil {
		log.Fatal(err)
	}

	var data string
	switch outputType {
	case "rss":
		data, err = feed.ToRss()
	case "atom":
		data, err = feed.ToAtom()
	case "json":
		data, err = feed.ToJSON()
	}
	if err != nil {
		log.Fatalln("exporting data:", err)
	}
	fmt.Fprintf(os.Stdout, "%s", data)
}
