package goquerylua

import (
	"testing"

	lua "github.com/yuin/gopher-lua"
)

func TestDoc(t *testing.T) {
	L := lua.NewState()
	defer L.Close()
	L.PreloadModule("goquery", Loader())

	if err := L.DoString(`
	goquery = require("goquery")

	doc, err = goquery.newDoc([[<html><body>
		<div class="x">
			<a href="https://gitlab.com/microo8/photon">photon</a>
		</div>
		<div class="x">
			<a href="https://gitlab.com/microo8/ratt">ratt</a>
		</div>
	</body></html>]])
	if err ~= nil then
		error(err)
	end
	doc:find(".x a"):each(function(i, s)
		print(s:attr("href"))
	end)
	`); err != nil {
		t.Fatal(err)
	}
}

func TestDocURL(t *testing.T) {
	L := lua.NewState()
	defer L.Close()
	L.PreloadModule("goquery", Loader())

	if err := L.DoString(`
	goquery = require("goquery")

	doc, err = goquery.newDocFromURL("http://motherfuckingwebsite.com/")
	if err ~= nil then
		error(err)
	end
	doc:find("blockquote"):each(function(i, s)
		print(s:text())
	end)
	`); err != nil {
		t.Fatal(err)
	}
}
