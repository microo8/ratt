package goquerylua

import (
	"net/http"

	lua "github.com/yuin/gopher-lua"
)

type settings struct {
	client *http.Client
}

func Loader(options ...Option) lua.LGFunction {
	s := &settings{}
	for _, o := range options {
		o(s)
	}
	if s.client == nil {
		s.client = http.DefaultClient
	}
	var exports = map[string]lua.LGFunction{
		"newDoc":        newDoc(s),
		"newDocFromURL": newDocFromURL(s),
	}
	return func(L *lua.LState) int {
		mod := L.SetFuncs(L.NewTable(), exports)
		L.Push(mod)

		registerDocumentType(L)
		registerSelectionType(L)

		return 1
	}
}

type Option func(*settings)

func HTTPClient(c *http.Client) func(s *settings) {
	return func(s *settings) {
		s.client = c
	}
}
