package gojqlua

import (
	"encoding/json"

	"github.com/itchyny/gojq"
	lua "github.com/yuin/gopher-lua"
)

func Loader() lua.LGFunction {
	var exports = map[string]lua.LGFunction{
		"parse": parse,
	}
	return func(L *lua.LState) int {
		mod := L.SetFuncs(L.NewTable(), exports)
		L.Push(mod)

		registerQueryType(L)

		return 1
	}
}

func parse(L *lua.LState) int {
	query, err := gojq.Parse(L.ToString(1))
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}
	ud := L.NewUserData()
	ud.Value = query
	L.SetMetatable(ud, L.GetTypeMetatable(QUERY_TYPENAME))
	L.Push(ud)
	L.Push(lua.LNil)
	return 2
}

const QUERY_TYPENAME = "query"

var queryMethods = map[string]lua.LGFunction{
	"runMap":   queryRunMap,
	"runArray": queryRunArray,
}

func registerQueryType(L *lua.LState) {
	mt := L.NewTypeMetatable(QUERY_TYPENAME)
	L.SetField(mt, "__index", L.SetFuncs(L.NewTable(), queryMethods))
}

func checkQuery(L *lua.LState) *gojq.Query {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*gojq.Query); ok {
		return v
	}
	L.ArgError(1, "query expected")
	return nil
}

func queryRunMap(L *lua.LState) int {
	query := checkQuery(L)
	data := L.ToString(2)
	var jsonData map[string]interface{}
	if err := json.Unmarshal([]byte(data), &jsonData); err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}
	queryRun(L, query, jsonData)
	return 2
}

func queryRunArray(L *lua.LState) int {
	query := checkQuery(L)
	data := L.ToString(2)
	var jsonData []interface{}
	if err := json.Unmarshal([]byte(data), &jsonData); err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}
	queryRun(L, query, jsonData)
	return 2
}

func queryRun(L *lua.LState, query *gojq.Query, jsonData interface{}) int {
	iter := query.Run(jsonData)
	var res []interface{}
	for {
		v, ok := iter.Next()
		if !ok {
			break
		}
		if err, ok := v.(error); ok {
			L.Push(lua.LNil)
			L.Push(lua.LString(err.Error()))
			return 2
		}
		res = append(res, v)
	}
	L.Push(fromGo(L, res))
	L.Push(lua.LNil)
	return 1
}

func fromGo(L *lua.LState, value interface{}) lua.LValue {
	switch converted := value.(type) {
	case bool:
		return lua.LBool(converted)
	case float64:
		return lua.LNumber(converted)
	case int:
		return lua.LNumber(converted)
	case string:
		return lua.LString(converted)
	case []interface{}:
		arr := L.CreateTable(len(converted), 0)
		for _, item := range converted {
			arr.Append(fromGo(L, item))
		}
		return arr
	case map[string]interface{}:
		tbl := L.CreateTable(0, len(converted))
		for key, item := range converted {
			tbl.RawSetH(lua.LString(key), fromGo(L, item))
		}
		return tbl
	}

	return lua.LNil
}
