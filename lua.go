package main

import (
	"fmt"
	"strings"

	"github.com/PuerkitoBio/goquery"
	lua "github.com/yuin/gopher-lua"
	gojqlua "gitlab.com/microo8/ratt/gojq-lua"
	goquerylua "gitlab.com/microo8/ratt/goquery-lua"
)

func runLua(sel *goquery.Selection, code string, url string, luaOptions ...luaOption) (string, error) {
	L := lua.NewState()
	defer L.Close()

	for _, o := range luaOptions {
		o(L)
	}

	//require('goquery')
	if err := loadModule(L, "goquery", goquerylua.Loader(goquerylua.HTTPClient(GlobalHTTPSettings.Client()))); err != nil {
		return "", err
	}
	if err := loadModule(L, "gojq", gojqlua.Loader()); err != nil {
		return "", err
	}
	logs := overwritePrint(L)

	L.SetGlobal("setGlobal", L.NewFunction(setGlobal))
	for k, v := range luaGlobal {
		L.SetGlobal(k, v)
	}

	//set global selection
	ud := L.NewUserData()
	ud.Value = sel
	L.SetMetatable(ud, L.GetTypeMetatable(goquerylua.SELECTION_TYPENAME))
	L.SetGlobal("sel", ud)
	L.SetGlobal("url", lua.LString(url))

	//run Lua code
	if err := L.DoString(code); err != nil {
		return "", fmt.Errorf("running lua script: %w\n%s", err, code)
	}

	return logs.String(), nil
}

type luaOption func(*lua.LState)

func addGlobal(name string, value lua.LValue) luaOption {
	return func(L *lua.LState) {
		L.SetGlobal(name, value)
	}
}

var luaGlobal = make(map[string]lua.LValue)

func setGlobal(L *lua.LState) int {
	key := L.ToString(1)
	value := L.Get(2)
	luaGlobal[key] = value
	return 0
}

func loadModule(L *lua.LState, name string, fn lua.LGFunction) error {
	L.PreloadModule(name, fn)
	if err := L.CallByParam(lua.P{
		Fn:      L.GetGlobal("require"),
		NRet:    1,
		Protect: true,
	}, lua.LString(name)); err != nil {
		return err
	}
	mod := L.Get(-1)
	L.Pop(1)
	L.SetGlobal(name, mod)
	return nil
}

func overwritePrint(L *lua.LState) *strings.Builder {
	logs := &strings.Builder{}
	L.SetGlobal("print", L.NewFunction(func(L *lua.LState) int {
		var args []string
		for i := 1; i <= L.GetTop(); i++ {
			args = append(args, L.Get(i).String())
		}
		logs.WriteString(strings.Join(args, " "))
		return 0
	}))
	return logs
}
